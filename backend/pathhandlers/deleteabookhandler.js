//importing Bookmodel 
import { BookModel } from "../model/model.js";

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const delete_book_handler=(req,res)=>{
    //accessing id from req
    const {id} = req.params;
   
    //deleting a book based on id
    BookModel.deleteOne({_id:id},(err,data)=> {
        //throwing err on unsuccesfull deletion of a book
        if (err) {
           res.status(400).send({message: "Can not be deleted"})
         }
       else {
        //sending res code 200 on succesfull deletion of a book
           res.status(200).send( {message:"Deleted Successfully"})
        }
    })
}

//exporting handler function
export{delete_book_handler};
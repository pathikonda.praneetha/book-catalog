//importing bookmodel
import {BookModel} from "../model/model.js"
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const add_book_hanlder = (req, res) => {
  //accessing data from req
  const { name, description, authorname, price, publishername,availability  } = req.body;
  
  const book_details = { name, description, authorname, price, publishername,availability };

  //creating an instance of BookModel
  const new_book = new BookModel(book_details);

  // saving the book details into the database
 new_book.save((err, result) => {
    if (err) {
      //throwing error on unsuccessful addition of book
      res.status(500).send("Unable to add book, server error! " + err);
    } else {
      //sending res code 200 on succesfull addition of book
      res.status(200).send("Book Added successfully");
    }
  });
};

//exporting handler function
export {add_book_hanlder};

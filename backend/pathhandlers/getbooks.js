
//importing bookmodel
import { BookModel } from "../model/model.js";
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 */
const get_books_handler = (req, res) => {
  //retriving all books using find
  BookModel.find((err, data) => {
    if (err) {
      //throwing error code 400 and msg as no book found on not finding a book
      res.status(404).send("No Book Found");
    } else if (data) {
      //sending response code 200 and sending data on successfull attempt
      res.status(200).send(data);
    }
  });
};


//exporting function
export { get_books_handler };

//importing Bookmodel
import { BookModel } from "../model/model.js";

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 */
const get_book_handler=(req,res)=>{
    //getting id from params
    const { id } = req.params;
    //finding one book based on id
    BookModel.findOne({ _id:id}, (err, data) => {
        if (err) {
            res.status(404).send("Data Not Fetched");
        } else {
            //ensuring book is found based on id
            res.status(200).send(data)
        }
    });
}



//exporting function
export{get_book_handler};


   
  
  
  

  
  
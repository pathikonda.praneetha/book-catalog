//importing Bookmodel
import { BookModel } from "../model/model.js";

/**
 *
 * @param {*} req
 * @param {*} res
 *
 */
const edit_books_handler = (req, res) => {
  //getting name and price from req
  const {
    id,
    name,
    price,
    availability,
    authorname,
    publishername,
    descrtiption,
  } = req.body;
  console.log(req.body)
  //finding one book based on id
  BookModel.updateOne(
    { _id: id },
    {
      $set: {
        price: price,
        availability: availability,
        authorname: authorname,
        publishername: publishername,
        descrtiption: descrtiption,
      }

      },(err,data)=>{
        if(err){
            res.status(500).send({message:"Details not updated"})
        }else{
          res.status(200).send({message:"Details are updated"})
        }
    }
  );
};

//exporting function
export { edit_books_handler };

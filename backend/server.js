// Importing External Dependencies

import express from "express";
import bodyParser from "body-parser";
import mongoose  from "mongoose";
import dotenv from "dotenv";
import cors from "cors";


//importing internal dependencies
import { add_book_hanlder } from "./pathhandlers/addbook.js";
import{get_books_handler} from "./pathhandlers/getbooks.js";
import { edit_books_handler } from "./pathhandlers/editbookdetails.js";
import { delete_book_handler } from "./pathhandlers/deleteabookhandler.js";

//importing validations
import db_unique_validator from "./database_validation/db_validator.js";
import { get_book_handler } from "./pathhandlers/getbookbyid.js";




//for reading constants from .env file
dotenv.config(); 


//creating an instance of express
const app = express();
// Returns middleware that only parses json
app.use(bodyParser.json()); 
// Retruns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({extended:true}));
// cors using
app.use(cors({origin:"*"}))

//performing rest api calls

//posting books 
app.post("/post_a_book", db_unique_validator, add_book_hanlder);
//retriving boooks
app.get("/get_books",get_books_handler);
//retriving book details by id
app.get("/get_book_by_id/:id",get_book_handler)
//uodating books details
app.put("/edit_book_details",edit_books_handler);
//deleting a book
app.delete("/delete_a_book/:id",delete_book_handler);

//connecting to mongodb using the url fetched from .env file
mongoose
    .connect(process.env.MONGO_URL)
    //saving data on successful attempt
    .then(()=> {
        app.listen(process.env.PORT, ()=> {
            console.log("Server Running Sucessfully on PORT " + process.env.PORT)
        })
    })
    //throwing error on unsuccesfull attempt
    .catch((err)=> {
        console.log("Unable to connect MongoDB." + err)
    })
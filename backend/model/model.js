// import mongoose
import mongoose from "mongoose";

/**
 * Schema for creating book
 */
const BookSchema = new mongoose.Schema({
    name: {type:String, required:true, unique: true},
    description: {type:String, required:true},
    availability: {type:Boolean, required:true,default:true},
    price: {type:Number, required:true},
    authorname: {type:String, required:true},
    publishername: {type:String, required:true}
})

//creating bookmodel
const BookModel = mongoose.model("BookModel",BookSchema);


//exporting model
export {BookModel};


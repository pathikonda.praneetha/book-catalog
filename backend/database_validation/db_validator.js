// importing BookModel
import { BookModel } from "../model/model.js";

/**
 * This handler is used for checking uniqueness of the book
 * @param {*} requests name as body params for verifying if the book is already present in database
 * @param {*} response sends status_code: 409 if the book is there in database, otherwise
 *                     calls middleware function next() for further process
 */
const db_unique_validator = (req, res, next) => {
  const { name} = req.body;

  BookModel.find({name:name},(err,data)=> {
    if (err) {
        res.status(404).send("Something went wrong")
    }
    else {
        //checking whether the book entered is there in store or not
        if (data.length === 0) {
            next()
        }
        else {
            res.status(409).send("Book is already present in store")
        }
    }
})

  
};

export default db_unique_validator;
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from 'src/compons/home/home';
import { AddbookComponent } from 'src/compons/addbook/addbook';
import { DeletebookComponent } from 'src/compons/deletebook/deletebook';
import { EditbookComponent } from 'src/compons/editbook/editbook';



const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'addbook',component:AddbookComponent},
  {path:'deletebook/:id',component:DeletebookComponent},
  {path:'editbook/:id',component:EditbookComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const RoutingComponents=[HomeComponent,AddbookComponent,DeletebookComponent,EditbookComponent]
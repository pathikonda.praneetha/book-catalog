import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-hello',
  templateUrl: './deletebook.html',
  styleUrls: ['./deletebook.css'
  ]
})
export class DeletebookComponent implements OnInit {
   
  book:any={name:"",id:"",_id:"",authorName:"",publisherName:"",description:"",price:"",availability:""}

  id=this.route.snapshot.paramMap.get('id');
  url:string="http://localhost:2856/get_book_by_id/"+this.id;

  constructor(private route:ActivatedRoute,private http:HttpClient,private router:Router) { 
     this.http.get(this.url).subscribe((data)=>{
       this.book=data
      //  console.log(data)
      //  console.log(this.id)
       this.book.id=this.id
     })
  }

  ngOnInit(): void {
  }

  deleteBook(){
    console.log(this.book)
    const headers={'Content-Type':'application/json','Accept':'application/json'};
    this.http.delete(`http://localhost:2856/delete_a_book/${this.id}`,{headers}).subscribe(
      (response=>{
       console.log(response)
       alert("Book deleted Successfully!")
       this.router.navigateByUrl("")
      }),(
        error=>{
          console.error("Book Is Not Deleted!!")
          alert("Book is not deleted")
        })
      
    )

  }

}


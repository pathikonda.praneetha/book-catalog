import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-hello',
  templateUrl:'./editbook.html',
  styleUrls:['./editbook.css']

})
export class EditbookComponent implements OnInit {

  id: any 


books:any={name: "", authorname: "", publishername: "", description: "", availability: "", price: "", id: ""};
  constructor(private http:HttpClient,private activatedRoute: ActivatedRoute,) {
    this.ngOnInit()
    const get_url:string="http://localhost:2856/get_book_by_id/"+ this.id;
    http.get(get_url).subscribe((data)=> {
      this.books = data
      this.books.id =  this.id
    })

    // http.put(this.get_url)
  }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    })
    
  }
  
  clickFunction():void {
    this.http.put<any>('http://localhost:2856/edit_book_details', this.books).subscribe(data => {
      console.log(data)
    })
  }

  getName(event:any) {
    this.books.name = event.target.value
  }

  getDesc(event:any) {
    this.books.description = event.target.value
  }

  getAuthorName(event:any) {
    this.books.authorname = event.target.value
  }

  getPublisherName(event:any) {
    this.books.publishername = event.target.value
  }

  getAvailability(event:any) {
    this.books.availability = event.target.value
  }

  getPrice(event:any) {
    this.books.price = event.target.value
  }


}

import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventManager } from '@angular/platform-browser';

@Component({
  selector: 'app-hello',
  templateUrl:'./addbook.html',
  styleUrls:['./addbook.css']

})
export class AddbookComponent implements OnInit {

  bookDetails = {name: "", authorname: "", publishername: "", description: "", availability: "", price: ""}

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }

  clickFunction():void {
    this.http.post<any>('http://localhost:2856/post_a_book', this.bookDetails).subscribe(data => {
      console.log(data)
    })
  }

  getName(event:any) {
    this.bookDetails.name = event.target.value
  }

  getDesc(event:any) {
    this.bookDetails.description = event.target.value
  }

  getAuthorName(event:any) {
    this.bookDetails.authorname = event.target.value
  }

  getPublisherName(event:any) {
    this.bookDetails.publishername = event.target.value
  }

  getAvailability(event:any) {
    this.bookDetails.availability = event.target.value
  }

  getPrice(event:any) {
    this.bookDetails.price = event.target.value
  }






}

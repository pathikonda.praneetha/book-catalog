import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-Home',
  templateUrl: "./home.html",
  styleUrls: ["./home.css"
  ]
})
export class HomeComponent implements OnInit {

  url: string = "http://localhost:2856/get_books";
  books: any = ""

  constructor(private http:HttpClient) {
     http.get(this.url).subscribe((data)=> {
       this.books = data
     })
   }

  ngOnInit(): void {
    
  }

}

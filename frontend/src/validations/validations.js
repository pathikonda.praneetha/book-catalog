//authorname,publishername,availability
const validate_name=(name)=>{
    if(name.length===0){
        return "*Book name should not be empty"
    } 
    else if (!( /^[a-zA-Z0-9'-_ ]+$/.test(name))){
        return "*Book name should be in valid format"
     }
     else if (name.length <5 || name.length > 30)
     {
        return "*Book name must have more than 4 char and less than 30"
     }
     else {
         return ""
     }
}

const validate_description=(description)=>{
    if(description.length===0){
        return"*Desctiption should not be empty!"
    }
    else if(description.length<30 || description.length>250){
        return "*Description should be 30-250 characters length!"
    }
    else{
        const desc_arr = description.split(" ")
        for (let word of desc_arr) {
             if (word.length > 20) {
                 return "*Word should not be more than 25 chars"
                }
            }
        return ""
    }

}



const validate_price=(price)=>{
    if (price.length === 0) {
        return "*Price should not be empty"
    }
    else if (isNaN(price)) {
        return "*Price should be a Number"
    }
    else {
        const price_num = parseInt(price)
        if (price_num<0 || price_num> 10000) {
            return "*Price should not be less than zero or more than 10000."
        }
        else {
            return ""
        }
    }
}



const validate_author_name=(author)=>{
    if(author.length===0){
        return"*Author Name is Required";
    }
    else if (author.length<5 || author.length >18) {
        return "*Author Name should be between 5 to 18 characters"
    }
    else if (!( /^[a-zA-Z'-_ ]+$/.test(author))) {
        return "*Author Name should be in valid format"
    }
    else {
        return ""
    }
}


const valiadate_publisher = (publisher) => {
    if (publisher.length === 0) {
        return "*Publisher Name is Required"
    }
    else if (publisher.length<8 || publisher.length> 30) {
        return "*Publisher Name should be between 8 to 30 characters"
    }
    else if (!( /^[a-zA-Z0-9'-_ ]+$/.test(publisher))) {
        return "*Publisher Name should be in valid format"
    }
    else {
        return ""
    }
}

const validate_availability = (availability) => {
    if (availability.length === 0) {
            return "*Availability should not be empty"
    }
    else if (availability.toLowerCase() === "true" || availability.toLowerCase() === "false") {
        return ""
    }
    else {
        return "*Availability should be either 'true' or 'false'"
    }
}

export {validate_name,validate_description,validate_price, validate_author_name,  valiadate_publisher, validate_availability};
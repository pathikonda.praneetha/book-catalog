import React, { Component } from 'react';
import {withRouter }from "react-router-dom"

import "./deletebook.css"

import Topbar from '../topbar/topbar';

class Deletebook extends Component {
    constructor(props) {
        super(props);
        this.state = {
          name: "",
          id: "",
          authorname: "",
          msg: "",
        };
      }
    

      componentDidMount() {
        this.getServerData();
      }
    
      getServerData=async()=>{
        let id = this.props.match.params.id;
        
        this.setState({id: id });
        console.log(id)
      
        const url="http://localhost:2856/get_book_by_id/" + id
        const option={
          method:"GET",
          headers:{
            "Content-type":"application/json",
            "Accept":"application/json"
          }
    
        }
    
        const response=await fetch(url,option)
        if (response.status===200){
          const data = await response.json()
          
          this.setState({
            name:data.name,
            authorname: data.authorname,
          })
        }
      }

      redirectpage = () => {
        const {history} = this.props;
        history.replace("/")
      }
      

      submit = async (event) => {
        event.preventDefault();
    
        this.setState({ msg: "" });
        const { id } = this.state;
        const Book_Data = { id };
        
          const url = "http://localhost:2856/delete_a_book";
    
          const options = {
            method: "DELETE",
            body: JSON.stringify(Book_Data),
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
            },
          };
    
          try {
            const response = await fetch(url, options);
    
            if (response.status === 200) {
              const msg = "Book Deleted Successfully";
              alert(msg)
              this.setState({ msg: msg });
            }
             else {
              const msg = "Book Not Deleted";
              alert(msg)
              this.setState({ msg: msg });
            }
          } catch {
            const msg = "Unable to connect the server";
            alert(msg)
            this.setState({ msg: msg });
          }
          
          this.redirectpage()
        
      };

    render() {
        const {name,authorname, msg} = this.state;
        return (
            <>
            <Topbar/>
            <div className="container_form">
              <div className='details_bg'>
                <p className="book_name">Book Name: {name}</p>
                <p className="book_name">Author: {authorname}</p>
              </div>
              <p className='confirmation'>*Click to confirm</p>
                <p className='message text-center'>{msg}</p>
                <div className="add_button">
                <button className="button_style mr-4 pl-3 pr-3" onClick={this.redirectpage}> Back </button>
                <button className="button_style" onClick={this.submit}> Delete  </button>
                </div>
            </div>
            </>
        );
    }
    
}

export default withRouter(Deletebook);
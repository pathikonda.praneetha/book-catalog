import React, { Component } from 'react'

import { Link } from 'react-router-dom'

export default class Topbar extends Component {
  render() {
    return (
      <>
    <div className="nav_bar d-none d-md-block">
        <div className="d-flex flex-row">
          <div className="nav_bar_image">
            <img alt=""
              src="https://assets.vogue.com/photos/594830328ba2821cdbcb950f/master/w_1600%2Cc_limit/01-best-new-york-city-bookstores.jpg"
              className="nav_bar_image"
            />
          </div>
          <div className="nav_bar_data">
            <h1 className="main_name">Welcome</h1>
            <p className="nav_bar_quote">
              A good book acts as a window to the light of wisdom
            </p>
          </div>
        </div>
    </div>
    <div className="nav_bar d-block d-md-none">
        <div className="d-flex flex-row justify-content-center">
          
          <div className="nav_bar_data">
            <h1 className="main_name">Welcome</h1>
            <p className="text-center nav_quote_medium">
              "A good book acts as a window to the light of wisdom"
            </p>
          </div>
        </div>
    </div>
    <div className="top_nav_bar">
        <Link to="/">
      <button className="m-2 fontcolour">
        <i className="fas fa-list" />
        <span className='d-none d-md-inline'>Books</span>
      </button>
        </Link>
        <Link to='/add_book'>
        <button className="m-2 fontcolour">
          <i className="fas fa-folder-plus" /> 
          <span className='d-none d-md-inline'>Add Book{" "}</span>
        </button>
        </Link>    
{/*   
  <div className='d-flex flex-row justify-content-end p-3 bg-search container-fluid'>    
  <form className="d-flex">
  <input className="form-control btn-size" type="search" placeholder="Search" aria-label="Search"/>
  <button className="btn btn-primary btn-size" type="submit">
  <i className='fas fa-search'></i>
  </button>
  </form>
  </div> */}
  </div>
  </>
    )
  }
}

import { Link } from "react-router-dom";
import ReadMoreReact from "read-more-react";

const HomePageHelper = (props) => {
  const book = props.each;
  let availability = "";
  let delete_book = "/delete_book/" + book._id;
  let update_book = "/update_book/" + book._id;
  if (book.availability === true) {
    availability = "Available";
  } else {
    availability = "Out of Stock";
  }
  return (
    <div className="col-12 col-md-4">
      <div className="bdetailsbox ml-3 mr-3 mb-5 p-4">
        <h1 className="btitle">
          <span>Name:</span> {book.name}
        </h1>
        <p className="bdescription">
          <span>Description:</span> 
          <span>
          <ReadMoreReact text={book.description}
                min={20}
                ideal={40}
                max={100}
                readMoreText="Read more"
          />
          </span>
        </p>

        <p className="price">
          <span>Price:</span> Rs. {book.price} /-
        </p>
        <p className="authname">
          <span>Author Name:</span> {book.authorname}
        </p>
        <p className="pubname">
          <span>Publisher Name:</span> {book.publishername}
        </p>
        <p className="pubname">
          <span>Availability:</span> {availability}
        </p>

        <Link to={update_book}>
          <a className="m-2 fontcolour">
            <i className="icon fas fa-edit fa-2x" />
          </a>
        </Link>

        <Link to={delete_book}>
          <a className="m-2 fontcolour">
            <i className="icon fas fa-trash-alt fa-2x" />
          </a>
        </Link>
      </div>
    </div>
  );
};

export default HomePageHelper;

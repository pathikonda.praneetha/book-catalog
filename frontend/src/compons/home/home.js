import React, { Component } from "react";
import Topbar from "../topbar/topbar";
import "./home.css";

import HomePageHelper from "./homepagehelper.js";

class Home extends Component {
  state = {
    list_of_books: [],
    search: "",
    books: [],
    display_all: "d-block",
    display_search: "d-none"
  };

  submitSearch = (event) => {
    event.preventDefault();
    this.componentDidMount();
    this.SearchBooks();
  };

  SearchBooks = () => {
    let filtered_data = []

    for (const each of this.state.list_of_books) {
      const book_name = each.name.toLowerCase();
      
        if (book_name.includes(this.state.search.toLowerCase())) {
          filtered_data.push(each)
        }
    }
    this.setState({list_of_books:[]})
    this.setState({books:filtered_data, display_all: "d-none", display_search:"d-block"})
  }


  onChangeSearch = (event) => {
    this.setState({ search: event.target.value });
  };

  componentDidMount() {
    this.getServerData();
  }

  getServerData = async () => {
    const url = "http://localhost:2856/get_books";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    this.setState({
     list_of_books: data
    });
  };



  render() {
    const { list_of_books, search , display_all, display_search, books} = this.state;
    return (
      <>
        <Topbar />
        <div className="main_sec ">
          <div className="d-flex flex-row justify-content-end mb-3 pt-3">
            <div className="home_heading">
              <span className="welcome_msg_home ">
                {" "}
                Books{" "}
                <span className="welcome_msg_home d-none d-md-inline">
                  in the Store{" "}
                </span>{" "}
              </span>
            </div>

            <div className="d-flex flex-row justify-content-end p-3 mr-2 container-fluid">
              <form className="d-flex">
                <input
                  className="form-control btn-size"
                  type="search"
                  placeholder="Search Book"
                  aria-label="Search"
                  onChange={this.onChangeSearch}
                  value={search}
                />
                <button
                  className="btn-primary-button  btn-size"
                  type="submit"
                  onClick={this.submitSearch}
                >
                  <i className="fas fa-search"></i>
                </button>
              </form>
            </div>
          </div>
          <div className={display_all}>
          <div className="container_flex">
            {list_of_books.map((book) => (
              <HomePageHelper key={book.id} each={book} />
            ))}
          </div>
          </div>
          <div className={display_search}>
          <div className="container_flex">
            {books.map((book) => (
              <HomePageHelper key={book.id} each={book} />
            ))}
          </div>
          </div>
        </div>
      </>
    );
  }
}

export default Home;

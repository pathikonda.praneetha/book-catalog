import React, { Component } from "react";
import "./addbook.css";
import Topbar from "../topbar/topbar";
import { validate_name, valiadate_publisher, validate_author_name, validate_availability, validate_description, validate_price } from "../../validations/validations.js";

class Addbook extends Component {

    constructor(props) {
        super(props);
        this.state = {
                name: "",
                description: "",
                authorname: "",
                publishername: "",
                price: "",
                availability: "",
                suc_msg: "",
                err_msg:"",
                else_msg:"",
                name_err:"",
                description_err: "",
                publisher_err: "",
                author_name_err: "",
                price_err: "",
                availability_err: ""
            };
      }
    
      onChangeBookName = (event) => {
        this.setState({name:event.target.value, suc_msg: "", err_msg: "", else_msg: ""})
      }
    
      onChangeBookDescription = (event) => {
        this.setState({description:event.target.value, suc_msg: "", err_msg: "", else_msg: ""})
      }
    
      onChangeAuthor = (event) => {
        this.setState({authorname: event.target.value, suc_msg: "", err_msg: "", else_msg: ""})
      }
    
      onChangePublisher = (event) => {
        this.setState({publishername: event.target.value, suc_msg: "", err_msg: "", else_msg: ""})
      }
    
      onChangeBookPrice = (event) => {
        this.setState({price: event.target.value, suc_msg: "", err_msg: "", else_msg: ""})
      }
    
      onChangeAvailablity = (event) => {
        const availability = event.target.value.toLowerCase()
        this.setState({availability: availability, suc_msg: "", err_msg: "", else_msg: ""})
      }


      clearForm = () => {
        this.setState({name: "",
        description: "",
        authorname: "",
        publishername: "",
        price: "",
        availability: "",
        suc_msg: "",
        err_msg:"",
        else_msg:"",
        name_err:"",
        description_err: "",
        publisher_err: "",
        author_name_err: "",
        price_err: "",
        availability_err: ""})
      }
    
      submitForm = async (event) => {
        
         event.preventDefault()
         
         const name_err=validate_name(this.state.name);
         this.setState({name_err:name_err})
         
         const author_name_err = validate_author_name(this.state.authorname)
         this.setState({author_name_err:author_name_err})

         const publisher_err = valiadate_publisher(this.state.publishername)
         this.setState({publisher_err:publisher_err})

         const price_err = validate_price(this.state.price) 
         this.setState({price_err:price_err})

         const description_err = validate_description(this.state.description)
         this.setState({description_err:description_err})

         const availability_err = validate_availability(this.state.availability) 
         this.setState({availability_err:availability_err})

        this.setState({ suc_msg: "" });
        this.setState({ err_msg: "" });
        this.setState({ else_msg: "" });

        const {name, authorname, description, publishername, price, availability} = this.state
    
        const Book_Data = {name, authorname, description, publishername, price, availability}
    
        const url = "http://localhost:2856/post_a_book"
        
        const options = {
          method: "POST",
          body: JSON.stringify(Book_Data),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
        };

          if (name_err === "" && description_err === "" && availability_err === "" && author_name_err === "" && publisher_err === "" && price_err === "") {
            try {
              const response = await fetch(url, options);
            
              if (response.status === 200) {
                const suc_msg = "Book Added Successfully to Database"
                this.setState({suc_msg: suc_msg})
              }
              else {
                const err_msg = "Book Not Added to Database"
                this.setState({err_msg: err_msg})
              }
            }
            catch {
              const else_msg = "Unable to connect the backend server"
              this.setState({else_msg: else_msg})
            }
          }
        
    
    }    

  render() {
      const {name, authorname, description, publishername, price,suc_msg,err_msg,else_msg,name_err, description_err, availability_err, author_name_err, price_err, publisher_err} = this.state
    return (
      <>
        <Topbar />
        <div className="add_main_sec col-12">
          <h1 className="welcome_msg"> Add a Book </h1>
          <div className="d-flex flex-row justify-content-center">
            <form className="book_form col-7 col-md-6">
              <label htmlFor="name" className="label_book mt-3 mb-1">
                {" "}
                Name
              </label>
              <input className="input " type="text" name="name" value={name} onChange={this.onChangeBookName}/>
              <p className="mb-0 err_message">{name_err} </p>
              <label htmlFor="book_desc" className="label_book mt-3 mb-1">
                {" "}
                Description
              </label>
              <input className="input" type="text" name="book_desc" onChange={this.onChangeBookDescription} value={description}/>
              <p className="mb-0 err_message">{description_err} </p>
              <label htmlFor="book_author" className="label_book mt-3 mb-1">
                Author of the Book
              </label>
              <input className="input pr-2" type="text" name="book_author" onChange={this.onChangeAuthor} value={authorname} />
              <p className="mb-0 err_message">{author_name_err} </p>
              <label htmlFor="book_publisher" className="label_book mt-3 mb-1">
                {" "}
                Publisher
              </label>
              <input className="input" type="text" name="book_publisher" onChange={this.onChangePublisher} value={publishername}/>
              <p className="mb-0 err_message">{publisher_err} </p>
              <label htmlFor="book_price" className="label_book mt-3 mb-1">
                {" "}
                Price{" "}
              </label>
              <input className="input" type="text" name="book_price" onChange={this.onChangeBookPrice} value={price} />
              <p className="mb-0 err_message">{price_err} </p>


              <p className="label_book mt-3 mb-1"> Availability </p>
              <input type="radio" id="true" value="true" name="avail" onChange={this.onChangeAvailablity}></input>
              <label for="true" className="label_book_radio"> Yes </label>
              <input type="radio" className="ml-5" id="false" value="false" name="avail" onChange={this.onChangeAvailablity}></input>
              <label for="false" className="label_book_radio"> No </label>

              <p className="mb-0 err_message">{availability_err} </p>
            </form>
          </div>
          {/*displaying success/failure messege */}
          <p className='message text-center green'>{suc_msg}</p>
          <p className='message text-center red'>{err_msg}</p>
          <p className='message text-center red'>{else_msg}</p>
          <div className="add_button mt-4 ">
            {/*calling submitform function on clicking the button*/}
            <button className="button_style mb-4 mr-4" onClick={this.clearForm}> Clear All </button>
            <button className="button_style mb-4" onClick={this.submitForm}> Submit </button>
          </div>
        </div>
      </>
    );
  }
}



export default Addbook;

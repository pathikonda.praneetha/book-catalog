import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import "./UpdateBookPrice.css";

import { valiadate_publisher, validate_author_name, validate_description, validate_price } from "../../validations/validations.js";

import Topbar from "../topbar/topbar";

class UpdateBookPrice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      id:"",
      description: "",
      authorname: "",
      publishername: "",
      price: "",
      availability: "true",
      suc_msg: "",
      err_msg: "",
      else_msg: "",
      name_err: "",
      description_err: "",
      publisher_err: "",
      author_name_err: "",
      price_err: "",
      availability_err: "",
    };
  }

 
  onChangeBookDescription = (event) => {
    this.setState({ description: event.target.value, suc_msg: "", err_msg: "", else_msg: "" });
  };

  onChangeAuthor = (event) => {
    this.setState({ authorname: event.target.value, suc_msg: "", err_msg: "", else_msg: "" });
  };

  onChangePublisher = (event) => {
    this.setState({ publishername: event.target.value, suc_msg: "", err_msg: "", else_msg: "" });
  };

  onChangeBookPrice = (event) => {
    this.setState({ price: event.target.value, suc_msg: "", err_msg: "", else_msg: "" });
  };

  onChangeAvailablity = (event) => {
    const availability = event.target.value.toLowerCase();
    this.setState({ availability: availability, suc_msg: "", err_msg: "", else_msg: "" });
  };

  componentDidMount() {
    this.getServerData();
  
  }

  getServerData=async()=>{
    let id = this.props.match.params.id;
    
    this.setState({id: id });
    console.log(id)
  
    const url="http://localhost:2856/get_book_by_id/" + id
    const option={
      method:"GET",
      headers:{
        "Content-type":"application/json",
        "Accept":"application/json"
      }

    }

    const response=await fetch(url,option)
    if (response.status===200){
      const data = await response.json()
      
      this.setState({
        name:data.name,
        description: data.description,
        price: data.price,
        stock: data.stock,
        authorname: data.authorname,
        publishername: data.publishername,
        availability: data.availability
      })
    }
  }
  
  submit = async (event) => {
    event.preventDefault();
         
         const author_name_err = validate_author_name(this.state.authorname)
         this.setState({author_name_err:author_name_err})

         const publisher_err = valiadate_publisher(this.state.publishername)
         this.setState({publisher_err:publisher_err})

         const price_err = validate_price(this.state.price) 
         this.setState({price_err:price_err})

         const description_err = validate_description(this.state.description)
         this.setState({description_err:description_err})

         console.log(author_name_err)

    this.setState({ msg: "" });

    const { id, name, price, availability, authorname, publishername, description } = this.state;

    const Book_Data = { name, price, id, availability, authorname, publishername, description };

    const url = "http://localhost:2856/edit_book_details";
    const options = {
      method: "PUT",
      body: JSON.stringify(Book_Data),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };

    if (description_err === ""  && author_name_err === "" && publisher_err === "" && price_err === "") {
      try {
        const response = await fetch(url, options);
  
        if (response.status === 200) {
          const msg = "Deatails Updated Successfully";
          this.setState({ suc_msg: msg });
        } else {
          const msg = "Book Not Updated";
          this.setState({ err_msg: msg });
        }
      } catch {
        const msg = "Unable to connect the server";
        this.setState({ else_msg: msg });
      }
    }

  };

  render() {
    const {
      name,
      authorname,
      description,
      publishername,
      price,
      suc_msg,
      err_msg,
      else_msg,
      description_err,
      availability_err,
      author_name_err,
      price_err,
      publisher_err,
    } = this.state;
    return (
      <>
        <Topbar />

        <div className="add_main_sec col-12">
          <h1 className="welcome_msg"> Edit Details </h1>
          <div className="d-flex flex-row justify-content-center">
            <form className="book_form col-7 col-md-6">
              <label htmlFor="name" className="label_book mt-3 mb-1">
                {" "}
                Name
              </label>
              <p>{name}</p>

              <label htmlFor="book_desc" className="label_book mt-3 mb-1">
                {" "}
                Description
              </label>
              <input
                className="input"
                type="text"
                name="book_desc"
                onChange={this.onChangeBookDescription}
                value={description}
              />
              <p className="mb-0 err_message">{description_err} </p>
              <label htmlFor="book_author" className="label_book mt-3 mb-1">
                Author of the Book
              </label>
              <input
                className="input pr-2"
                type="text"
                name="book_author"
                onChange={this.onChangeAuthor}
                value={authorname}
              />
              <p className="mb-0 err_message">{author_name_err} </p>
              <label htmlFor="book_publisher" className="label_book mt-3 mb-1">
                {" "}
                Publisher
              </label>
              <input
                className="input"
                type="text"
                name="book_publisher"
                onChange={this.onChangePublisher}
                value={publishername}
              />
              <p className="mb-0 err_message">{publisher_err} </p>
              <label htmlFor="book_price" className="label_book mt-3 mb-1">
                {" "}
                Price{" "}
              </label>
              <input
                className="input"
                type="text"
                name="book_price"
                onChange={this.onChangeBookPrice}
                value={price}
              />
              <p className="mb-0 err_message">{price_err} </p>

              <p className="label_book mt-3 mb-1"> Availability </p>
              <input
                type="radio"
                id="true"
                value="true"
                name="avail"
                onChange={this.onChangeAvailablity}
              ></input>
              <label for="true" className="label_book_radio">
                {" "}
                Yes{" "}
              </label>
              <input
                type="radio"
                className="ml-5"
                id="false"
                value="false"
                name="avail"
                onChange={this.onChangeAvailablity}
              ></input>
              <label for="false" className="label_book_radio">
                {" "}
                No{" "}
              </label>

              <p className="mb-0 err_message">{availability_err} </p>
            </form>
          </div>
          {/*displaying success/failure messege */}
          <p className="message text-center green">{suc_msg}</p>
          <p className="message text-center red">{err_msg}</p>
          <p className="message text-center red">{else_msg}</p>
          <div className="add_button mt-4 ">
            {/*calling submitform function on clicking the button*/}
            <button className="button_style mb-4" onClick={this.submit}>
              {" "}
              Submit{" "}
            </button>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(UpdateBookPrice);

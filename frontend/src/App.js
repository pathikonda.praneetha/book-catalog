import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./compons/home/home";
import Deletebook from "./compons/deletebook/deletebook";
import Addbook from "./compons/addbook/addbook";
import UpdateBookPrice from "./compons/UpdateBookPrice/UpdateBookPrice";

function App() {
  return (
    
     <Router>
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route exact path="/add_book">
            <Addbook/>
          </Route>
          <Route exact path="/delete_book/:id">
            <Deletebook/>
          </Route>
          <Route exact path="/update_book/:id">
            <UpdateBookPrice/>
          </Route>
        </Switch>
      </Router>

    
  );
}

export default App;